package com.example.demo.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "custom_user")
public class CustomUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private Long userFbId;
    private String name;
    private String last_name;
    private String first_name;
    private String gender;
    private String profilePictureUrl;
}
