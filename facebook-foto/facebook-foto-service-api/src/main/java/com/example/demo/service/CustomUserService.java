package com.example.demo.service;

import com.example.demo.entity.CustomUser;

public interface CustomUserService {

    void saveCustomUser(CustomUser user);

    CustomUser findCustomUserByFbId(Long fbId);

    void deletUserByUserFbId(Long userFbId);
}

