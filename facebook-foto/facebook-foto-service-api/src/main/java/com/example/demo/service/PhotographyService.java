package com.example.demo.service;

import com.example.demo.entity.Photography;

import java.util.List;

public interface PhotographyService {

    void deletePhotographiesByUserFbId(Long userFbId);

    void saveUserTaggedPhotography(Photography photographies);

    List<Photography> getUserPhotographiesByUserFbId(Long userFbId);
 }
