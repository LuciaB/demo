package com.example.demo.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "photography")
public class Photography {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long photographyId;
    private Long userFbId;
    private String fbUrl ;
    private String imageFileUrl;
    private String albumName;
    private Integer numberOfReactions;
}
