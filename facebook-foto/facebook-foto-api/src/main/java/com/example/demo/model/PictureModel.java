package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PictureModel {

    @Facebook("data")
    private DataModel data;
}
