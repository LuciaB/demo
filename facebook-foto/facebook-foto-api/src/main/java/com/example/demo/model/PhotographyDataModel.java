package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhotographyDataModel {

    private Long userFbId;

    @Facebook("id")
    private String photoFbId;

    @Facebook("link")
    private String fbUrl ;

    @Facebook("images")
    private ImageModel[] imageFileUrls;

    @Facebook("album")
    private FacebookAccessInfo.AlbumModel albumName;


    private Integer numberOfReactions;
}
