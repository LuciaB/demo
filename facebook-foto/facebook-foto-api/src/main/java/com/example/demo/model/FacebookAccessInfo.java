package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

@Builder
@Getter
@Setter
public class FacebookAccessInfo {

    private Long userId;

    private String userAccessToken;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AlbumModel {

        @Facebook("name")
        private String albumName;
    }
}
