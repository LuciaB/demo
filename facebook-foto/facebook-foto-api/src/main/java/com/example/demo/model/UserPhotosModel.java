package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPhotosModel {

    @Facebook("photos")
    private List<PhotographyModel> photos;
}
