package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataModel {

    @Facebook("url")
    private String fbProfilePictureUrl;
}
