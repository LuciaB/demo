package com.example.demo.dto;

import com.restfb.Facebook;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PhotographyDto {


    private Long userFbId;

    private String fbUrl ;

    private String imageFileUrl;

    private String albumName;

    private Integer numberOfReactions;
}
