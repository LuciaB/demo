package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomUserModel {
    @Facebook("id")
    private Long userFbId;
    @Facebook("name")
    private String name;
    @Facebook("last_name")
    private String last_name;
    @Facebook("first_name")
    private String first_name;
    @Facebook("gender")
    private String gender;
    @Facebook("picture")
    private PictureModel profilePicture;
}
