package com.example.demo.model;

import com.restfb.Facebook;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhotographyModel {

    @Facebook("data")
    private PhotographyDataModel[] photographies;
}
