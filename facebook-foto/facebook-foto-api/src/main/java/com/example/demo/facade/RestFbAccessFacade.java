package com.example.demo.facade;

import com.example.demo.model.FacebookAccessInfo;

public interface RestFbAccessFacade {

    void getConnectionAndUserData(FacebookAccessInfo accessInfo);

}
