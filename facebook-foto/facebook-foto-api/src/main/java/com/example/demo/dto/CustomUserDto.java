package com.example.demo.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CustomUserDto {
    private Long userFbId;
    private String name;
    private String last_name;
    private String first_name;
    private String gender;
    private String profilePictureUrl;
}
