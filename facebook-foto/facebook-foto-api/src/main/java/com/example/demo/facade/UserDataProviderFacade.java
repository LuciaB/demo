package com.example.demo.facade;

import com.example.demo.dto.CustomUserDto;
import com.example.demo.dto.PhotographyDto;

import java.util.List;

public interface UserDataProviderFacade {

    CustomUserDto getUserInfo(Long userFbId);

    void deleteUserWithAllDataByUserFbId(Long userFbId);

    List<PhotographyDto>  getUsersPhotographyByUserFbId(Long userFbId);
}
