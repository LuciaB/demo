package com.example.demo;

import com.example.demo.facade.RestFbAccessFacade;
import com.example.demo.model.FacebookAccessInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/facebook-foto")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FacebookConnectionController {

    private final RestFbAccessFacade restFbAccessFacade;

    @PostMapping("/users")
    public ResponseEntity getConnectionAndUserData(@Valid @RequestBody FacebookAccessInfo accessInfo){
        restFbAccessFacade.getConnectionAndUserData(accessInfo);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

