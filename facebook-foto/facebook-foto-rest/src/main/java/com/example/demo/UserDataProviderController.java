package com.example.demo;

import com.example.demo.dto.CustomUserDto;
import com.example.demo.dto.PhotographyDto;
import com.example.demo.facade.UserDataProviderFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/facebook-foto")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDataProviderController {

    private final UserDataProviderFacade userDataProviderFacade;

    @GetMapping("/{user_fb_id}")
    public CustomUserDto getUserData(@PathVariable("user_fb_id") Long userFbId) {
        return userDataProviderFacade.getUserInfo(userFbId);
    }

    @GetMapping("/users/{user_fb_id}/photos")
    public List<PhotographyDto> getUserPhotography(@PathVariable("user_fb_id") Long userFbId) {
        return userDataProviderFacade.getUsersPhotographyByUserFbId(userFbId);
    }

    @DeleteMapping("/{user_fb_id}")
    public ResponseEntity deleteUderWithData(@PathVariable("user_fb_id") Long userFbId) {
        userDataProviderFacade.deleteUserWithAllDataByUserFbId(userFbId);
        return ResponseEntity.noContent().build();
    }

}
