package com.example.demo.facade;

import com.example.demo.converter.CustomUserDtoConverter;
import com.example.demo.converter.PhotographyDtoConverter;
import com.example.demo.dto.CustomUserDto;
import com.example.demo.dto.PhotographyDto;
import com.example.demo.service.CustomUserService;
import com.example.demo.service.PhotographyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDataProviderFacadeImpl implements UserDataProviderFacade {

    private final CustomUserDtoConverter customUserDtoConverter;
    private final CustomUserService customUserService;
    private final PhotographyService photographyService;
    private final PhotographyDtoConverter photographyModelConverter;

    @Override
    public CustomUserDto getUserInfo(Long userFbId) {
        return customUserDtoConverter.createDto(customUserService.findCustomUserByFbId(userFbId));
    }

    @Override
    @Transactional
    public void deleteUserWithAllDataByUserFbId(Long userFbId) {
        deleteUserWithAllData(userFbId);
    }

    @Override
    public List<PhotographyDto> getUsersPhotographyByUserFbId(Long userFbId) {
        return photographyModelConverter.createDtos(photographyService.getUserPhotographiesByUserFbId(userFbId));
    }


    private void deleteUserWithAllData(Long userFbId) {
        photographyService.deletePhotographiesByUserFbId(userFbId);
        customUserService.deletUserByUserFbId(userFbId);
    }
}
