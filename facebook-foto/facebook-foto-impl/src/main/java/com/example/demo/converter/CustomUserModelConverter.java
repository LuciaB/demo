package com.example.demo.converter;

import com.example.demo.model.CustomUserModel;
import com.example.demo.entity.CustomUser;
import org.springframework.stereotype.Component;

@Component
public class CustomUserModelConverter extends AbstractConverter<CustomUser, CustomUserModel> {
    @Override
    protected CustomUser toEntity(CustomUserModel dto) {
        return CustomUser.builder()
                .userFbId(dto.getUserFbId())
                .name(dto.getName())
                .last_name(dto.getLast_name())
                .first_name(dto.getFirst_name())
                .gender(dto.getGender())
                .profilePictureUrl(dto.getProfilePicture().getData().getFbProfilePictureUrl())
                .build();
    }

    @Override
    protected CustomUserModel toDto(CustomUser entity) {
        return CustomUserModel.builder()
                .userFbId(entity.getUserFbId())
                .name(entity.getName())
                .last_name(entity.getLast_name())
                .first_name(entity.getFirst_name())
                .gender(entity.getGender())
                .build();
    }
}
