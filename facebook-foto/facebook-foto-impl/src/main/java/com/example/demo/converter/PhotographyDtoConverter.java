package com.example.demo.converter;

import com.example.demo.dto.PhotographyDto;
import com.example.demo.entity.Photography;
import org.springframework.stereotype.Component;

@Component
public class PhotographyDtoConverter extends AbstractConverter<Photography, PhotographyDto> {


    @Override
    protected Photography toEntity(PhotographyDto dto) {
        return Photography.builder()
                .userFbId(dto.getUserFbId())
                .albumName(dto.getAlbumName())
                .fbUrl(dto.getFbUrl())
                .imageFileUrl(dto.getImageFileUrl())
                .numberOfReactions(dto.getNumberOfReactions())
                .build();
    }

    @Override
    protected PhotographyDto  toDto(Photography entity) {
        return PhotographyDto.builder()
                .userFbId(entity.getUserFbId())
                .albumName(entity.getAlbumName())
                .fbUrl(entity.getFbUrl())
                .imageFileUrl(entity.getImageFileUrl())
                .numberOfReactions(entity.getNumberOfReactions())
                .build();
    }
}
