package com.example.demo.converter;

import java.util.List;

public interface Converter<E, D> {
    E createEntity(D dto);

    List<E> createEntities(List<D> dtos);

    D createDto(E entity);

    List<D> createDtos(List<E> entities);
}
