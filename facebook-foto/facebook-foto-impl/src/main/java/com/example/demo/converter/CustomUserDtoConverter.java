package com.example.demo.converter;

import com.example.demo.dto.CustomUserDto;
import com.example.demo.entity.CustomUser;
import com.example.demo.model.CustomUserModel;
import org.springframework.stereotype.Component;

@Component
public class CustomUserDtoConverter extends AbstractConverter<CustomUser, CustomUserDto> {
    @Override
    protected CustomUser toEntity(CustomUserDto dto) {
        return CustomUser.builder()
                .userFbId(dto.getUserFbId())
                .name(dto.getName())
                .last_name(dto.getLast_name())
                .first_name(dto.getFirst_name())
                .gender(dto.getGender())
                .profilePictureUrl(dto.getProfilePictureUrl())
                .build();
    }

    @Override
    protected CustomUserDto toDto(CustomUser entity) {
        return CustomUserDto.builder()
                .userFbId(entity.getUserFbId())
                .name(entity.getName())
                .last_name(entity.getLast_name())
                .first_name(entity.getFirst_name())
                .gender(entity.getGender())
                .profilePictureUrl(entity.getProfilePictureUrl())
                .build();
    }
}
