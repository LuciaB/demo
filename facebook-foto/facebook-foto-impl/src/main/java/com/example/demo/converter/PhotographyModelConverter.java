package com.example.demo.converter;

import com.example.demo.model.PhotographyDataModel;
import com.example.demo.entity.Photography;
import org.springframework.stereotype.Component;

@Component
public class PhotographyModelConverter extends AbstractConverter<Photography, PhotographyDataModel> {

    @Override
    protected Photography toEntity(PhotographyDataModel dto) {
        return Photography.builder()
                .userFbId(dto.getUserFbId())
                .albumName(dto.getAlbumName().getAlbumName())
                .fbUrl(dto.getFbUrl())
                .imageFileUrl(dto.getImageFileUrls()[0].getImageFileUrl())
                .numberOfReactions(dto.getNumberOfReactions())
                .build();
    }

    @Override
    protected PhotographyDataModel toDto(Photography entity) {
        return PhotographyDataModel.builder()
                .userFbId(entity.getUserFbId())
                .fbUrl(entity.getFbUrl())
                .numberOfReactions(entity.getNumberOfReactions())
                .build();
    }
}
