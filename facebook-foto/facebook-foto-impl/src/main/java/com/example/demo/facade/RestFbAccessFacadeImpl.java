package com.example.demo.facade;

import com.example.demo.converter.CustomUserModelConverter;
import com.example.demo.converter.PhotographyModelConverter;
import com.example.demo.model.*;
import com.example.demo.service.CustomUserService;
import com.example.demo.service.PhotographyService;
import com.restfb.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RestFbAccessFacadeImpl implements RestFbAccessFacade {

    private final CustomUserService customUserService;
    private final CustomUserModelConverter customUserModelConverter;
    private final PhotographyModelConverter photographyModelConverter;
    private final PhotographyService photographyService;

    @Override
    public void getConnectionAndUserData(FacebookAccessInfo accessInfo) {
        FacebookClient client = new DefaultFacebookClient(accessInfo.getUserAccessToken(), Version.VERSION_3_3);
        CustomUserModel user = client.fetchObject("me", CustomUserModel.class, Parameter.with("fields", "id, name, last_name, first_name, gender, picture"), Parameter.with("redirect", "false"));
        customUserService.saveCustomUser(customUserModelConverter.createEntity(user));
        String photosQuery = String.format("%s/photos", user.getUserFbId().toString());
        Connection<UserPhotosModel> photographyConnection = client.fetchConnection(photosQuery, UserPhotosModel.class, Parameter.with("type", "tagged"), Parameter.with("redirect", "false"), Parameter.with("fields","id, album, link, images"));

        for (List<UserPhotosModel> photos : photographyConnection) {
            for (UserPhotosModel fotos : photos) {
                if (null != fotos.getPhotos()) {
                    for (PhotographyModel foto : fotos.getPhotos()) {
                        for (PhotographyDataModel image : foto.getPhotographies()) {
                            photographyService.saveUserTaggedPhotography(photographyModelConverter.createEntity(image));
                        }
                    }
                }
            }
        }
    }
}
