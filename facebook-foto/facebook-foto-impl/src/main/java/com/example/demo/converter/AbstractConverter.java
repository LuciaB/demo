package com.example.demo.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractConverter<E, D> implements Converter<E, D> {
    @Override
    public E createEntity(D dto){
        if(null == dto){
            return null;
        }
        return this.toEntity(dto);
    }

    @Override
    public List<E> createEntities(List<D> dtos){
        if(null == dtos){
            return new ArrayList<>();
        }
        return dtos.stream().map(this::createEntity).collect(Collectors.toList());
    }

    @Override
    public D createDto(E entity){
        if(null == entity){
            return null;
        }
        return this.toDto(entity);
    }

    @Override
    public List<D> createDtos(List<E> entities){
        if(null == entities){
            return new ArrayList<>();
        }
        return entities.stream().map(this::createDto).collect(Collectors.toList());
    }

    protected abstract E toEntity(D dto);

    protected abstract D toDto(E entity);
}
