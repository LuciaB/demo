package com.example.demo.service;

import com.example.demo.entity.Photography;
import com.example.demo.repository.PhotographyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PhotographyServiceImple implements PhotographyService {

    private final PhotographyRepository photographyRepository;


    @Override
    public void deletePhotographiesByUserFbId(Long userFbId) {
        photographyRepository.deleteAllByUserFbId(userFbId);
    }

    @Override
    public void saveUserTaggedPhotography(Photography photographies) {
        photographyRepository.save(photographies);
    }

    @Override
    public List<Photography> getUserPhotographiesByUserFbId(Long userFbId) {
        return photographyRepository.findAllByUserFbId(userFbId);
    }
}
