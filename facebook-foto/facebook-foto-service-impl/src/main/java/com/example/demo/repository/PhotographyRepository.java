package com.example.demo.repository;

import com.example.demo.entity.Photography;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PhotographyRepository extends JpaRepository<Photography, Long> {

    @Query
    void deleteAllByUserFbId(@Param("userFbId") Long userFbId);

    @Query
    List<Photography> findAllByUserFbId(@Param("userFbId") Long userFbId);
}
