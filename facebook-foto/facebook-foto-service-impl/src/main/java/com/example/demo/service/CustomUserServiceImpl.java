package com.example.demo.service;

import com.example.demo.entity.CustomUser;
import com.example.demo.repository.CustomUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomUserServiceImpl implements CustomUserService {

    private final CustomUserRepository customUserRepository;

    @Override
    public void saveCustomUser(CustomUser user) {
        customUserRepository.save(user);
    }

    @Override
    public CustomUser findCustomUserByFbId(Long fbId) {
        return customUserRepository.findCustomUserByFbId(fbId).get();
    }

    @Override
    public void deletUserByUserFbId(Long userFbId) {
        customUserRepository.deleteCustomUserByUserFbId(userFbId);
    }
}
