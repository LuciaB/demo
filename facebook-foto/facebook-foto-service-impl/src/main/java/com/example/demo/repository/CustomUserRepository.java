package com.example.demo.repository;

import com.example.demo.entity.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CustomUserRepository extends JpaRepository<CustomUser, Long> {

    @Query("SELECT u FROM CustomUser u  WHERE u.userFbId = :fbId")
    Optional<CustomUser> findCustomUserByFbId(@Param("fbId") Long fbId);

    @Query
    void deleteCustomUserByUserFbId(@Param("userFbId") Long userFbId);
}
