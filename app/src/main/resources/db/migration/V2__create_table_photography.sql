CREATE TABLE IF NOT EXISTS public.photography(
 photography_id serial PRIMARY KEY ,
 user_fb_id integer REFERENCES custom_user(user_fb_id),
 fb_url varchar(300) NOT NULL,
 image_file_url varchar(300) NOT NULL,
 album_name VARCHAR(150) DEFAULT NULL,
 number_of_reactions INTEGER DEFAULT NULL
);