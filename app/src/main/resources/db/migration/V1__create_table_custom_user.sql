CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE IF NOT EXISTS public.custom_user(
user_id serial PRIMARY KEY ,
 user_fb_id integer NOT NULL,
 name varchar(150) NOT NULL,
 last_name varchar(100) NOT NULL,
 first_name varchar(100) NOT NULL,
 gender varchar(100) DEFAULT NULL,
 profile_picture_url varchar(300) DEFAULT NULL
);

